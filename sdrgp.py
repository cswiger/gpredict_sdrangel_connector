#!/usr/bin/env python3

import socket
import json
import requests
import sys

if len(sys.argv) < 2 :
  print("Usage: sdrgp.py freq")
  sys.exit(-1)


basefreq = int(sys.argv[1])

deltafreq = 0 
api_url_base = 'http://127.0.0.1:8091/'
headers = {'Content-Type': 'application/json'}
api_url = '{0}sdrangel/deviceset/1/channel/0/settings'.format(api_url_base)

sock = socket.socket()
sock.bind(('127.0.0.1',4532))
sock.listen(1)
conn, addr = sock.accept()

while True:
  try:
    data = conn.recv(1024)
    if ( data.decode('UTF-8')[0] == 'F' ):
      # string could have 1, 2 or more spaces depending on number of digits - just get last array item [-1] after split
      freq = int(data.decode('UTF-8').split(' ')[-1].strip())
      print('F: '+str(freq))
      conn.send(bytes('RPRT 0\n'.encode('UTF-8')))
      deltafreq = freq - basefreq 
      response = requests.patch(api_url, headers=headers, json=json.loads('{"SSBModSettings": {"inputFrequencyOffset": %d},"channelType": "SSBMod"}' % deltafreq))


    if ( data.decode('UTF-8')[0] == 'f' ):
      conn.send( bytes( (str(freq)+'\n').encode('UTF-8') ) )
      print('f: '+str(freq))
  except:
    conn.shutdown(2)
    conn.close()
    sys.exit(0)


